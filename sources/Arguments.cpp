/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Arguments.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 21:17:11 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/21 19:17:15 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Arguments.hpp"
#include "Logs.hpp"

/*!
 * Trims the string from both ends.
 * @param s string to be trimmed.
 */
static inline void trim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch)
                                    { return !std::isspace(ch); }));
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch)
                         { return !std::isspace(ch); }).base(), s.end());
}

/*!
 * Parses the arguments and fills the structure.
 * Logs errors. Starts from 1, skipping the program name.
 * @param argc number of arguents from main(argc, argv)
 * @param argv null-terminated arguments from main(argc, argv)
 */
Arguments::Arguments(int argc, const char **argv)
{
    VLogReport(VLogLevels::Args, "Parsing command line");

    //Vectorizing
    std::vector<std::string> args;
    for (int ct = 1; (argc < 0 || ct < argc) && argv[ct]; ct++)
    {
        args.emplace_back(argv[ct]);
        trim(args.back());
    }

    _help = GetFlagValue(args, "-h", "--help", false);
    _filename = GetArgValue(args, "-f", "--file=");
    _heuristic = GetArgValue(args, "--heuristic=", "--heuristic=", 0);
    _size = GetArgValue(args, "-s", "--size=", -1);
    _shuffle = GetArgValue(args, "--shuffle=", "--shuffle=", -1);
}

/*!
 * Looks for the flag in command args. Returns true if present, defaultValue otherwise.
 * @param args arguments from command line
 * @param shortOpt option to look for in "-x" format
 * @param longOpt option to look for in "--xxxx" format
 * @param defaultValue returned value if option is not found.
 * @return true if found, default value otherwise.
 */
bool Arguments::GetFlagValue(const std::vector<std::string>& args,
                    const std::string& shortOpt,
                    const std::string& longOpt,
                    bool defaultValue)
{
    return std::find_if(args.begin(), args.end(),
                [shortOpt, longOpt](const auto& item)
                { return item == shortOpt || item == longOpt; }
                ) != args.end() ? true : defaultValue;
}

/*!
 * Looks for the parameter in command args. Returns string after = if present, defaultValue otherwise.
 * @param args arguments from command line
 * @param shortOpt option to look for in "-x" format
 * @param longOpt option to look for in "--xxxx=" format
 * @param defaultValue returned value if option is not found.
 * @return returns string after = in "--xxxx=qqqqq" format or next argument in "-x" format.
 * If there is no such argument, defaultValue is returned.
 */
std::string Arguments::GetArgValue(const std::vector<std::string>& args,
                    const std::string& shortOpt,
                    const std::string& longOpt,
                    const std::string& defaultValue)
{
    const auto it = std::find_if(args.begin(), args.end(),
        [shortOpt, longOpt](const auto& item)
        {
            return item == shortOpt || item.rfind(longOpt) != std::string::npos;
        });

    if (it == args.end())
        return defaultValue;

    if (*it == shortOpt && it + 1 != args.end()
        && !(it + 1)->empty() && (*(it + 1))[0] != '-')
        return *(it + 1);

    const auto pos = it->find_last_of('=');
    //Since the short option has already been checked,
    //no sense to check for the long one here.
    if (pos < it->length() - 1)
        return it->substr(pos + 1);

    LOG(ERROR) << "Missing argument for " << *it;
    return defaultValue;
}

/*!
 * Looks for the parameter in command args. Returns unsigned after = if present, defaultValue otherwise.
 * @param args arguments from command line
 * @param shortOpt option to look for in "-x" format
 * @param longOpt option to look for in "--xxxx=" format
 * @param defaultValue returned value if option is not found.
 * @return returns unsigned after = in "--xxxx=qqqqq" format or next argument in "-x" format.
 * If there is no such argument, defaultValue is returned.
 */
int Arguments::GetArgValue(const std::vector<std::string>& args,
                    const std::string& shortOpt,
                    const std::string& longOpt,
                    const int defaultValue)
{
    const auto str = GetArgValue(args, shortOpt, longOpt);

    if (str.empty())
        return defaultValue;

    if (std::isdigit(str[0]))
        return std::atoi(str.c_str());

    LOG(ERROR) << "Invalid argument for " << longOpt << ": " << str;
    return defaultValue;
}

std::string Arguments::PrintHelp()
{
    std::stringstream ss;
    ss << "NAME\n\t"
       << "npuzzle is a simple 8/15-puzzle solver and generator.\n\n";
    ss << "SYNOPSIS\n\t"
       << "npuzzle [-h] [--help] [-f file] [--file=file] [-s size] [--size=3..99] [--shuffle=number] [--heuristic=0..3]\n\n";
    ss << "DESCRIPTION\n\t"
       << "Long options have priority over short ones, last options have priority over first ones.\n\n\t"
       << "-h, --help\tprints this message.\n\t"
       << "-f, --file\tloads a puzzle from the file.\n\t"
       << "-s, --size\tgenerates a puzzle with given size, has no effect when loading from file.\n\t"
       << "--shuffle\tshuffles the puzzle given amount of times, has no effect when loading from file.\n\t"
       << "--heuristic\tsolves the puzzle using given heuristic.\n\n";
    ss << "FILE\n\t"
       << "Here is an example of valid puzzle file format:\n\t"
       << "# this is a comment\n\t"
       << "4\n\t"
       << "5  7  9 14\n\t"
       << "4  2 13 10\n\t"
       << "6  0  3 11\n\t"
       << "1  8 12 15\n\n";
    ss << "HEURISTIC\n\t"
       << "0\t\tManhattan distance(default)\n\t"
       << "1\t\tSnake distance\n\t"
       << "2\t\tMean squared distance\n\t"
       << "3\t\tEuclidian distance\n\n";
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, const Arguments &args)
{
    os  << "Help:      " << std::boolalpha << args._help << "\n"
        << "Filename:  " << args._filename << "\n"
        << "Heuristic: " << args._heuristic << "\n"
        << "Shuffle    :" << args._shuffle << "\n"
        << "Size       :" << args._size << std::endl;
    return os;
}
