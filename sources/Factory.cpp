/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Factory.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 17:33:39 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 12:05:32 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <random>
#include <easylogging++.h>

#include "Logs.hpp"
#include "Solver.hpp"
#include "Factory.hpp"

const int MinPuzzleSize = 3;
const int MaxPuzzleSize = 100;
const int MaxValidPuzzleSize = 5;

Graph Factory::BuildGraph(const Arguments& args)
{
    const bool shouldBeGenerated = args.filename().empty();
    const Puzzle start = shouldBeGenerated
        ? GenerateStart(args.size(), args.shuffle())
        : ReadStart(args.filename());
    //Only 4 types for now
    const auto heuristic = args.heuristic() >= 4 || args.heuristic() < 0
            ? Heuristic::Type::Manhatton : Heuristic::Type(args.heuristic());
    return {start, Puzzle(start.size()), heuristic, shouldBeGenerated};
}

Puzzle Factory::ReadStart(const std::string& filename)
{
    VLogReport(VLogLevels::Factory, std::string("Reading the puzzle from: ") + filename);
    std::fstream file(filename, std::fstream::in);

    LOG_IF(!file.is_open(), ERROR) << "The file cannot be opened.";
    LOG_IF(file.is_open() && !file.good(), ERROR) << "The file is no good.";

    Puzzle result;

    if (file.good())
        file >> result;

    return result;
}

Puzzle Factory::GenerateStart(int size, int moves)
{
    VLogReport(VLogLevels::Factory, "Shuffling the puzzle");
    std::default_random_engine generator((std::random_device())());

    if (size < MinPuzzleSize || size > MaxPuzzleSize)
    {
        LOG_IF(size < 0, ERROR) << "Seriously??? Size less than zero???";
        LOG_IF(size >= 0, ERROR) << "Size " << size << " is too "
            << (size > MaxPuzzleSize ? "big" : "small")
            << ". We will use a random one from "
            << MinPuzzleSize << " to " << MaxValidPuzzleSize << ".";
        size = std::uniform_int_distribution<int>(MinPuzzleSize, MaxValidPuzzleSize)(generator);
    }

    if (moves < 0)
    {
        moves = size * std::uniform_int_distribution<int>(1000, 3000)(generator)
            + std::uniform_int_distribution<int>(3000, 10000)(generator);
    }

    Puzzle result(size);

    LOG(INFO) << "Puzzle size is " << result.size();
    LOG(INFO) << "Shuffling using " << moves << " moves.";

    for (auto ct = 0; ct < moves; ct++)
    {
        const auto validNeighbours = result.GetValidMoves();
        std::uniform_int_distribution<unsigned> distribution(0, validNeighbours.size() - 1);
        result += validNeighbours[distribution(generator)];
    }
    return result;
}
