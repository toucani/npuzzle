/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Solver.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 22:05:45 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/15 12:58:25 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <queue>
#include <numeric>
#include <cassert>
#include <unordered_set>
#include <unordered_map>

#include "Logs.hpp"
#include "Solver.hpp"
#include "Heuristic.hpp"

bool Solver::IsSolvable(const Puzzle& puzzle)
{
    if (!puzzle.valid())
    {
        LOG(ERROR) << "Cannot check invalid puzzle!";
        return false;
    }

    const auto finish = Puzzle(puzzle.size());

    auto startInv = GetInversionCount(puzzle.GetRepresentation());
    auto finishInv = GetInversionCount(finish.GetRepresentation());

    assert(puzzle.size() == finish.size());
    const auto IsEven = [](const size_t value) { return (value & 1) == 0; };

    if (IsEven(puzzle.size()))
    {
        const auto zpS = puzzle.find(0);
        startInv += zpS.row * puzzle.size() + zpS.col;
        const auto zpF = finish.find(0);
        finishInv += zpF.row * puzzle.size() + zpF.col;
    }
    return !(IsEven(startInv) ^ IsEven(finishInv));
}

void Solver::Solve()
{
    const auto start = std::chrono::high_resolution_clock::now();
    //We're using A* algorithm
    std::unordered_map<Puzzle, Puzzle> wayFrom;
    std::unordered_map<Puzzle, uint_fast16_t> cost;
    std::priority_queue<PriorityPuzzle> frontier;

    frontier.emplace(_graph.GetStart(), 0);
    cost.emplace(_graph.GetStart(), 0);

    while (!frontier.empty())
    {
        const auto current = frontier.top().GetPuzzle();
        if (current == _graph.GetFinish())
        {
            break;
        }

        frontier.pop();

        for (const auto& neib : Graph::GetNeighbours(current))
        {
            if (wayFrom.find(neib) == wayFrom.end()
                || cost[neib] > cost[current] + 1)
            {
                frontier.emplace(neib, _graph.Heuristic(neib, _graph.GetFinish()));
                wayFrom[neib] = current;
                cost[neib] = cost[current] + 1;//Cost value is 1.
            }
        }
        _complMem = std::max(wayFrom.size() + frontier.size(), _complMem);
    }

    //Traversing back and compiling the path
    const Puzzle* md = &_graph.GetFinish();
    _result.clear();
    _result.insert(_result.begin(), *md);
    while (*md != _graph.GetStart() && wayFrom.find(*md) != wayFrom.end())
    {
        _result.insert(_result.begin(), wayFrom[*md]);
        md = &(wayFrom[*md]);
    }

    //Statistics
    const auto finish = std::chrono::high_resolution_clock::now();
    _time = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
    _complTime = wayFrom.size();
    _complMem = std::max(wayFrom.size() + frontier.size(), _complMem);
}

void Solver::PrintResult() const
{
    for (const auto &p : _result)
        std::cout << p << "\n";
}

void Solver::PrintStats() const
{
    std::cout << "Moves: " << _result.size() << "\n";
    std::cout << "Time taken: ";
    if (_time.count() < 1000)
    {
        std::cout << _time.count() << " milliseconds\n";
    }
    else
    {
        std::cout << std::chrono::duration_cast<std::chrono::seconds>(_time).count() << " seconds\n";
    }
    std::cout << "Complexity in time:   " << _complTime << " (number of examined sets)\n";
    std::cout << "Complexity in memory: " << _complMem << " (maximum number of represented sets)\n";
}

Puzzle::container_type::size_type Solver::GetInversionCount(const Puzzle::container_type &data)
{
    Puzzle::container_type::size_type inversions = 0;
    std::set<Puzzle::cell_type> set;
    set.insert(data.front().front());

    for (const auto& line: data)
        inversions += std::accumulate(line.begin(), line.end(), 0,
            [&set](const auto& prevValue, const auto& cellValue)
            {
                // Gotta skip 0, since empty tile cannot be
                // counted as one of the inversions.
                if (cellValue == 0)
                    return long(prevValue);

                set.insert(cellValue);
                return prevValue + std::distance(set.upper_bound(cellValue), set.end());
            });

    return inversions;
}
