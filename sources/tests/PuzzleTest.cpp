/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PuzzleTest.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 22:05:45 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/19 17:44:45 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <random>
#include <gtest/gtest.h>

#include "Puzzle.hpp"

TEST(Puzzle, Basic)
{
    std::hash<Puzzle> hasher;
    const Puzzle md{};
    EXPECT_TRUE(md.empty());
    EXPECT_FALSE(md.valid());
    EXPECT_EQ(md.size(), 0);
    EXPECT_EQ(hasher(md), hasher(md));

    {
        const Puzzle md2 = md;
        const Puzzle md3(md);

        EXPECT_EQ(md, md2);
        EXPECT_EQ(md, md3);
        EXPECT_EQ(md2, md3);
        EXPECT_EQ(hasher(md), hasher(md2));
        EXPECT_EQ(hasher(md), hasher(md3));
        EXPECT_EQ(hasher(md2), hasher(md3));
    }

    {
        const Puzzle md2(3);
        const Puzzle md3(md2);
        const Puzzle md4 = md2;

        EXPECT_TRUE(md2.valid());
        EXPECT_TRUE(md3.valid());
        EXPECT_TRUE(md4.valid());
        EXPECT_FALSE(md2.empty());
        EXPECT_FALSE(md3.empty());
        EXPECT_FALSE(md4.empty());
        EXPECT_EQ(md2.size(), 3);
        EXPECT_EQ(md3.size(), 3);
        EXPECT_EQ(md4.size(), 3);
        EXPECT_EQ(md2, md3);
        EXPECT_EQ(md2, md4);
        EXPECT_EQ(md3, md4);
        EXPECT_EQ(hasher(md2), hasher(md3));
        EXPECT_EQ(hasher(md2), hasher(md4));
        EXPECT_EQ(hasher(md3), hasher(md4));
    }

}

TEST(Puzzle, Fill)
{
    const auto CreateAndTest = [](const size_t size, const std::string& s)
    {
        std::stringstream ss;
        ss << s;
        Puzzle p2;
        ss >> p2;
        const Puzzle p1(size);
        EXPECT_EQ(p1.size(), size);
        EXPECT_EQ(p2.size(), size);
        EXPECT_EQ(p1, p2);
        EXPECT_EQ(p1.find(0), p2.find(0));
        EXPECT_EQ(p1.find(2), p2.find(2));
        EXPECT_EQ(p1.find(4), p2.find(4));
        EXPECT_EQ(p1.find(size), p2.find(size));
    };

    CreateAndTest(3, "3\n\
                      1 2 3\n\
                      8 0 4\n\
                      7 6 5\n");

    CreateAndTest(4, "4\n\
                       1  2  3 4\n\
                      12 13 14 5\n\
                      11  0 15 6\n\
                      10  9  8 7");

    CreateAndTest(5, "5\n\
                       1  2  3  4 5\n\
                      16 17 18 19 6\n\
                      15 24  0 20 7\n\
                      14 23 22 21 8\n\
                      13 12 11 10 9");

    CreateAndTest(6, "6\n\
                       1  2  3  4  5  6\n\
                      20 21 22 23 24  7\n\
                      19 32 33 34 25  8\n\
                      18 31  0 35 26  9\n\
                      17 30 29 28 27 10\n\
                      16 15 14 13 12 11");

    CreateAndTest(7, "7\n\
                       1  2  3  4  5  6  7\n\
                      24 25 26 27 28 29  8\n\
                      23 40 41 42 43 30  9\n\
                      22 39 48  0 44 31 10\n\
                      21 38 47 46 45 32 11\n\
                      20 37 36 35 34 33 12\n\
                      19 18 17 16 15 14 13");

    CreateAndTest(8, "8\n\
                       1  2  3  4  5  6  7  8\n\
                      28 29 30 31 32 33 34  9\n\
                      27 48 49 50 51 52 35 10\n\
                      26 47 60 61 62 53 36 11\n\
                      25 46 59  0 63 54 37 12\n\
                      24 45 58 57 56 55 38 13\n\
                      23 44 43 42 41 40 39 14\n\
                      22 21 20 19 18 17 16 15");

    CreateAndTest(15, "15\n\
                        1   2   3   4   5   6   7   8   9  10  11  12  13  14  15\n\
                       56  57  58  59  60  61  62  63  64  65  66  67  68  69  16\n\
                       55 104 105 106 107 108 109 110 111 112 113 114 115  70  17\n\
                       54 103 144 145 146 147 148 149 150 151 152 153 116  71  18\n\
                       53 102 143 176 177 178 179 180 181 182 183 154 117  72  19\n\
                       52 101 142 175 200 201 202 203 204 205 184 155 118  73  20\n\
                       51 100 141 174 199 216 217 218 219 206 185 156 119  74  21\n\
                       50  99 140 173 198 215 224   0 220 207 186 157 120  75  22\n\
                       49  98 139 172 197 214 223 222 221 208 187 158 121  76  23\n\
                       48  97 138 171 196 213 212 211 210 209 188 159 122  77  24\n\
                       47  96 137 170 195 194 193 192 191 190 189 160 123  78  25\n\
                       46  95 136 169 168 167 166 165 164 163 162 161 124  79  26\n\
                       45  94 135 134 133 132 131 130 129 128 127 126 125  80  27\n\
                       44  93  92  91  90  89  88  87  86  85  84  83  82  81  28\n\
                       43  42  41  40  39  38  37  36  35  34  33  32  31  30  29");

    CreateAndTest(16, "16\n\
                    1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16\n\
                    60  61  62  63  64  65  66  67  68  69  70  71  72  73  74  17\n\
                    59 112 113 114 115 116 117 118 119 120 121 122 123 124  75  18\n\
                    58 111 156 157 158 159 160 161 162 163 164 165 166 125  76  19\n\
                    57 110 155 192 193 194 195 196 197 198 199 200 167 126  77  20\n\
                    56 109 154 191 220 221 222 223 224 225 226 201 168 127  78  21\n\
                    55 108 153 190 219 240 241 242 243 244 227 202 169 128  79  22\n\
                    54 107 152 189 218 239 252 253 254 245 228 203 170 129  80  23\n\
                    53 106 151 188 217 238 251   0 255 246 229 204 171 130  81  24\n\
                    52 105 150 187 216 237 250 249 248 247 230 205 172 131  82  25\n\
                    51 104 149 186 215 236 235 234 233 232 231 206 173 132  83  26\n\
                    50 103 148 185 214 213 212 211 210 209 208 207 174 133  84  27\n\
                    49 102 147 184 183 182 181 180 179 178 177 176 175 134  85  28\n\
                    48 101 146 145 144 143 142 141 140 139 138 137 136 135  86  29\n\
                    47 100  99  98  97  96  95  94  93  92  91  90  89  88  87  30\n\
                    46  45  44  43  42  41  40  39  38  37  36  35  34  33  32  31");
}

TEST(Puzzle, Read)
{
    // ----------- Valid maps ------------- //

    const auto ReadValidMaps = [](const std::string &testNumber, const std::string &s)
    {
        const auto expectedResult = "3\n"
                                    "  6   4   1 \n"
                                    "  3   0   8 \n"
                                    "  5   2   7 \n";
        Puzzle p;
        ASSERT_TRUE(p.empty());

        std::stringstream is;
        is << s;
        is >> p;
        EXPECT_EQ(p.DumpRepresentation(), expectedResult) << "Valid maps, " << testNumber << " FAILED";
    };

    std::vector<std::string> _readValidMapsData =
            {
                    "3\n"
                    "6 4 1\n"
                    "3 0 8\n"
                    "5 2 7",

                    "3\n"
                    "# dummy comment\n"
                    "# dummy comment\n"
                    "6 4 1\n"
                    "# dummy comment\n"
                    "3 0 8\n"
                    "#\n"
                    "#\n"
                    "5 2 7",

                    "   # dummy comment\n"
                    "3\n"
                    "   # dummy comment\n"
                    "6 4 1\n"
                    "3 0 8\n"
                    "5 2 7",

                    "# dummy comment\n"
                    "3\n"
                    "# dummy comment\n"
                    "6 4 1 3 0 8 5 2 7\n"
            };

    for (auto index = 0u; index < _readValidMapsData.size(); ++index) {
        ReadValidMaps("test " + std::to_string(index), _readValidMapsData[index]);
    }


    // ----------- Invalid maps ------------- //

    const auto ReadInvalidMaps = [](const std::string &testNumber, const std::string &s)
    {
        Puzzle p;
        ASSERT_TRUE(p.empty());

        std::stringstream is;
        is << s;
        is >> p;
        EXPECT_TRUE(p.empty()) << "Invalid maps, " << testNumber << " FAILED";
    };

    std::vector<std::string> _readInvalidMapsData =
            {
                    "",

                    "5\n"
                    "6 4 1\n"
                    "3 0 8\n"
                    "5 2 7",

                    "# dummy comment\n"
                    "# dummy comment\n"
                    "# dummy comment\n"
                    "## dummy comment\n",

                    "2\n"
                    "6 4 1\n"
                    "3 0 8"
            };

    for (auto index = 0u; index < _readInvalidMapsData.size(); ++index) {
        ReadInvalidMaps("test " + std::to_string(index), _readInvalidMapsData[index]);
    }
}

TEST(Puzzle, WriteAndRead)
{
    for (auto ct = 3; ct < 16; ct++)
    {
        const Puzzle mdOut(ct);
        std::stringstream ss;
        ss << mdOut.DumpRepresentation();
        Puzzle mdIn;
        ss >> mdIn;
        EXPECT_EQ(mdIn, mdOut);
    }
}

TEST(Puzzle, Find)
{
    const Puzzle md(4);

    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(13), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(15), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(2, 1));
}

TEST(Puzzle, ValidMoves)
{
    const auto CheckIt = [](const size_t size)
    {
        const Puzzle md(size);

        EXPECT_TRUE(md.valid());
        EXPECT_TRUE(md.IsMoveValid(Traits::Move::Up));
        EXPECT_TRUE(md.IsMoveValid(Traits::Move::Right));
        EXPECT_TRUE(md.IsMoveValid(Traits::Move::Down));
        EXPECT_TRUE(md.IsMoveValid(Traits::Move::Left));
    };

    CheckIt(3);
    CheckIt(4);
    CheckIt(5);
    CheckIt(6);
    CheckIt(7);
    CheckIt(8);
    CheckIt(16);
}

TEST(Puzzle, MovingAround)
{
    Puzzle md(4);

    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(13), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(15), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(2, 1));

    md += Traits::Move::Up;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(0), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(15), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(13), Traits::XYPosition(2, 1));

    md += Traits::Move::Right;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(0), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(15), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(13), Traits::XYPosition(2, 1));

    md += Traits::Move::Down;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(15), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(13), Traits::XYPosition(2, 1));

    md += Traits::Move::Left;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(15), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(13), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(2, 1));

    md -= Traits::Move::Up;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(15), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(13), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(2, 1));

    md -= Traits::Move::Left;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(0), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(15), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(13), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(2, 1));

    md -= Traits::Move::Down;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(13), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(15), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(2, 1));

    md -= Traits::Move::Right;
    EXPECT_EQ(md.find(1), Traits::XYPosition(0, 0));
    EXPECT_EQ(md.find(2), Traits::XYPosition(0, 1));
    EXPECT_EQ(md.find(3), Traits::XYPosition(0, 2));
    EXPECT_EQ(md.find(4), Traits::XYPosition(0, 3));
    EXPECT_EQ(md.find(5), Traits::XYPosition(1, 3));
    EXPECT_EQ(md.find(6), Traits::XYPosition(2, 3));
    EXPECT_EQ(md.find(7), Traits::XYPosition(3, 3));
    EXPECT_EQ(md.find(13), Traits::XYPosition(3, 2));
    EXPECT_EQ(md.find(8), Traits::XYPosition(3, 1));
    EXPECT_EQ(md.find(10), Traits::XYPosition(3, 0));
    EXPECT_EQ(md.find(11), Traits::XYPosition(2, 0));
    EXPECT_EQ(md.find(12), Traits::XYPosition(1, 0));
    EXPECT_EQ(md.find(14), Traits::XYPosition(1, 1));
    EXPECT_EQ(md.find(15), Traits::XYPosition(1, 2));
    EXPECT_EQ(md.find(9), Traits::XYPosition(2, 2));
    EXPECT_EQ(md.find(0), Traits::XYPosition(2, 1));
}
