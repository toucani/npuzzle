/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TraitsTest.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 20:15:10 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/17 14:14:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>

#include "Traits.hpp"

using namespace Traits;

TEST(XYPosition, Basic)
{
    XYPosition p;
    EXPECT_EQ(p.col, 0);
    EXPECT_EQ(p.row, 0);

    p.col = 7;
    p.row = 10;

    XYPosition p2(p);
    EXPECT_EQ(p2.col, 7);
    EXPECT_EQ(p2.row, 10);

    p2.col = 3;
    p2.row = 2;
    p = p2;

    EXPECT_EQ(p.col, 3);
    EXPECT_EQ(p.row, 2);
}

TEST(XYPosition, ManhattanDistance)
{
    const auto TestMHDistance = []
            (const Traits::XYPosition& p1,
            const Traits::XYPosition& p2,
            unsigned distance)
    {
        EXPECT_EQ(p1.Distance(p2), distance);
        EXPECT_EQ(p2.Distance(p1), distance);
    };

    TestMHDistance({5, 5}, {0, 0}, 10);
    TestMHDistance({0, 0}, {5, 5}, 10);
    TestMHDistance({10, 5}, {5, 10}, 10);
    TestMHDistance({1, 1}, {0, 0}, 2);
    TestMHDistance({0, 0}, {0, 0}, 0);
}

TEST(Move, Invert)
{
    EXPECT_EQ(Move::Up, !Move::Down);
    EXPECT_EQ(!Move::Up, Move::Down);
    EXPECT_EQ(Move::Right, !Move::Left);
    EXPECT_EQ(!Move::Right, Move::Left);
}
