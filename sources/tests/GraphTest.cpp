/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   GraphTest.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 21:29:58 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/21 08:17:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>

#include "Graph.hpp"

TEST(Graph, GetNeighbours)
{
    Puzzle start(4);
    Graph g(start, Puzzle(4));
    const auto moves = start.GetValidMoves();
    const auto nbhs = g.GetNeighbours(start);
    EXPECT_EQ(moves.size(), nbhs.size());
    for(const auto neighbour : nbhs)
    {
        const auto it = std::find_if(moves.begin(), moves.end(),
            [&start, &neighbour](const auto& move)
            {return start + move == neighbour;});
        EXPECT_TRUE(it != moves.end());
    }
}
