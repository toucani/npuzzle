/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SolverTest.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/12 16:56:55 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/04 09:48:05 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <easylogging++.h>

#include "Solver.hpp"
#include "Arguments.hpp"
#include "Factory.hpp"

INITIALIZE_EASYLOGGINGPP

TEST(Solver, InversionCount)
{
    //The data is taken from https://www.geeksforgeeks.org/check-instance-15-puzzle-solvable/

    Puzzle::container_type data = {{2, 1, 3},
                                  {4, 5, 6},
                                  {7, 8, 9}};
    EXPECT_EQ(Solver::GetInversionCount(data), 1);

    data = {{9, 8, 7},
            {6, 5, 4},
            {3, 2, 1}};
    EXPECT_EQ(Solver::GetInversionCount(data), 36);

    data = {{1, 8, 2},
            {0, 4, 3},
            {7, 6, 5}};
    EXPECT_EQ(Solver::GetInversionCount(data), 10);


    data = {{13, 2, 10, 3},
            {1, 12, 8, 4},
            {5, 0, 9, 6},
            {15, 14, 11, 7}};
    EXPECT_EQ(Solver::GetInversionCount(data), 41);


    data = {{6, 13, 7, 10},
            {8, 9,11, 0},
            {15, 2, 12, 5},
            {14, 3, 1, 4}};
    EXPECT_EQ(Solver::GetInversionCount(data), 62);
}

static const std::string currentFolder(SOURCE_PATH);

TEST(Solver, IsSolvable)
{
    const auto TestFiles = [](const std::string& folder, const bool success)
    {
        for (int size = 3; size < 11; size++)
            for (int iter = 1; iter < 50; iter++)
            {
                std::stringstream file;
                file << folder << "puzzle" << size << "_" << iter << ".in";
                const auto str = file.str();
                const char *av[4] = {"a.out", "-f", str.c_str()};
                Arguments args(-1, av);
                Solver s(Factory::BuildGraph(args));
                EXPECT_EQ(s.IsSolvable(), success) << "Error on file: " << file.str();
            }
    };

    TestFiles(currentFolder + "/sources/tests/slvbl/", true);
    TestFiles(currentFolder + "/sources/tests/uslvbl/", false);
}
