/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HeuristicTest.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 18:56:17 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/22 21:27:16 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>

#include "Puzzle.hpp"
#include "Heuristic.hpp"

TEST(Heuristic, ManhattanDistance)
{
    const auto CheckIt = [](const unsigned size, const std::string& str, unsigned value)
    {
        std::stringstream ss;
        ss << str;
        Puzzle m2;
        ss >> m2;
        const Puzzle m1(size);
        EXPECT_TRUE(m1.valid());
        EXPECT_TRUE(m2.valid());
        EXPECT_EQ(Heuristic::ManhattanDistance(m1, m2), value);
    };

    CheckIt(3, "3\n1 2 3\n8 0 4\n7 6 5", 0);
    CheckIt(3, "3\n1 2 3\n8 0 4\n7 5 6", 2);
    CheckIt(3, "3\n3 2 1\n0 4 8\n6 7 5", 10);
    CheckIt(3, "3\n8 0 4\n1 2 3\n7 5 6", 8);
}

TEST(Heuristic, SnakeDistance)
{
    const auto CheckIt = [](const unsigned size, const std::string& str, unsigned value)
    {
        std::stringstream ss;
        ss << str;
        Puzzle m2;
        ss >> m2;
        const Puzzle m1(size);
        EXPECT_TRUE(m1.valid());
        EXPECT_TRUE(m2.valid());
        EXPECT_EQ(Heuristic::SnakeDistance(m1, m2), value);
    };

    CheckIt(3, "3\n1 2 3\n8 0 4\n7 6 5", 0);

    //distance from 5 to its place is 1 * (9 - 5) = 4;
    //distance from 6 to its place is 1 * (9 - 6) = 3;
    CheckIt(3, "3\n1 2 3\n8 0 4\n7 5 6", 7);
    CheckIt(3, "3\n3 2 1\n0 4 8\n6 7 5", 49);
    CheckIt(3, "3\n8 0 4\n1 2 3\n7 5 6", 43);
}

TEST(Heuristic, MeanSquaredDistance)
{
    const auto CheckIt = [](const unsigned size, const std::string& str, unsigned value, unsigned index)
    {
        std::stringstream ss;
        ss << str;
        Puzzle m2;
        ss >> m2;
        const Puzzle m1(size);
        EXPECT_TRUE(m1.valid());
        EXPECT_TRUE(m2.valid());
        EXPECT_EQ(Heuristic::MeanSquaredDistance(m1, m2), value) << "Test # " << index << " failed";
    };

    CheckIt(3, "3\n1 2 3\n8 0 4\n7 6 5", 0, 1);
    CheckIt(3, "3\n1 2 3\n8 0 4\n7 5 6", 1, 2);
    CheckIt(3, "3\n3 2 1\n0 4 8\n6 7 5", 2, 3);
    CheckIt(3, "3\n8 0 4\n1 2 3\n7 5 6", 1, 4);
}


TEST(Heuristic, EuclidianDistance)
{
    const auto CheckIt = [](const unsigned size, const std::string& str, unsigned value, unsigned index)
    {
        std::stringstream ss;
        ss << str;
        Puzzle m2;
        ss >> m2;
        const Puzzle m1(size);
        EXPECT_TRUE(m1.valid());
        EXPECT_TRUE(m2.valid());
        EXPECT_EQ(Heuristic::EuclidianDistance(m1, m2), value) << "Test # " << index << " failed";
    };

    CheckIt(3, "3\n1 2 3\n8 0 4\n7 6 5", 0, 1);
    CheckIt(3, "3\n1 2 3\n8 0 4\n7 5 6", 1, 2);
    CheckIt(3, "3\n3 2 1\n0 4 8\n6 7 5", 4, 3);
    CheckIt(3, "3\n8 0 4\n1 2 3\n7 5 6", 3, 4);
}
