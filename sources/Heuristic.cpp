/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Heuristic.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <okosiako@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 14:27:23 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/22 21:07:43 by okosiako         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <numeric>
#include <cmath>

#include "Puzzle.hpp"
#include "Heuristic.hpp"

Heuristic::value_type Heuristic::ManhattanDistance(const Puzzle& current, const Puzzle& target)
{
    value_type result = 0;

    assert(current.valid() && target.valid());
    assert(current.size() == target.size());

    for (const auto& line : target.GetRepresentation())
        result = std::accumulate(line.begin(), line.end(), result,
            [&target, &current]
            (const auto& previousDistance, const auto& currentValue)
            {
                const auto targetPos = target.find(currentValue);
                const auto currentPos = current.find(currentValue);
                return previousDistance + targetPos.Distance(currentPos);
            });

    return result;
};

Heuristic::value_type Heuristic::SnakeDistance(const Puzzle& current, const Puzzle& target)
{
    value_type result = 0;
    const auto maxValue = current.size() * current.size();

    assert(current.valid() && target.valid());
    assert(current.size() == target.size());

    for (const auto& line : target.GetRepresentation())
        result = std::accumulate(line.begin(), line.end(), result,
            [&target, &current, &maxValue]
            (const auto& previousDistance, const auto& currentValue)
            {
                const auto targetPos = target.find(currentValue);
                const auto currentPos = current.find(currentValue);
                return previousDistance + (targetPos.Distance(currentPos) * (maxValue - currentValue));
            });

    return result;
}

Heuristic::value_type Heuristic::MeanSquaredDistance(const Puzzle &current, const Puzzle &target)
{
    value_type result = 0;
    auto amount = 0u;

    assert(current.valid() && target.valid());
    assert(current.size() == target.size());

    for (const auto& line : target.GetRepresentation())
        result = std::accumulate(line.begin(), line.end(), result,
            [&target, &current, &amount]
            (const auto& previousDistance, const auto& currentValue)
            {
                const auto targetPos = target.find(currentValue);
                const auto currentPos = current.find(currentValue);
                const auto res = targetPos.Distance(currentPos);
                amount += res != 0 ? 1 : 0;
                return previousDistance + (res * res);
            });

    return amount != 0 ? static_cast<value_type>(std::round(result / amount)) : amount;
}

Heuristic::value_type Heuristic::EuclidianDistance(const Puzzle &current, const Puzzle &target)
{
    value_type result = 0;

    assert(current.valid() && target.valid());
    assert(current.size() == target.size());

    for (const auto& line : target.GetRepresentation())
        result = std::accumulate(line.begin(), line.end(), result,
            [&target, &current]
            (const auto& previousDistance, const auto& currentValue)
            {
                const auto targetPos = target.find(currentValue);
                const auto currentPos = current.find(currentValue);
                const auto res = targetPos.Distance(currentPos);
                return previousDistance + (res * res);
            });

    return static_cast<value_type>(std::round(std::sqrt(result)));
}
