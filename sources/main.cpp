/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/18 22:11:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/15 13:05:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <easylogging++.h>

#include "Solver.hpp"
#include "Factory.hpp"

INITIALIZE_EASYLOGGINGPP

void SetUpLogger()
{
    el::Configurations defaultConf;
    defaultConf.setToDefault();

    defaultConf.setGlobally(el::ConfigurationType::ToFile, "true");
    defaultConf.setGlobally(el::ConfigurationType::Filename, "npuzzle.log");
    defaultConf.setGlobally(el::ConfigurationType::ToStandardOutput, "true");
    defaultConf.setGlobally(el::ConfigurationType::SubsecondPrecision, "2");
    defaultConf.setGlobally(el::ConfigurationType::MaxLogFileSize, "2097152");//2mb
    defaultConf.setGlobally(el::ConfigurationType::LogFlushThreshold, "50");
    defaultConf.setGlobally(el::ConfigurationType::Format, "[%datetime] [%level] %msg");
    defaultConf.set(el::Level::Debug, el::ConfigurationType::Format,
                    "Debug log: %func in %file @ %line: %msg");
    defaultConf.set(el::Level::Verbose, el::ConfigurationType::Format,
                    "[%datetime] [%level - %vlevel] %msg");

    el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
    el::Loggers::reconfigureLogger("default", defaultConf);
}

static Arguments GetArguments(int argc, const char **argv)
{
    Arguments ar(argc, argv);
    if (ar.help())
    {
        std::cout << Arguments::PrintHelp();
        std::exit(EXIT_SUCCESS);
    }
    return ar;
}

static Graph GetGraph(const Arguments& ar)
{
    auto graph = Factory::BuildGraph(ar);
    if (!graph.GetStart().valid())
    {
        LOG(ERROR) << "Puzzle is invalid.";
        std::exit(EXIT_FAILURE);
    }
    return graph;
}

static void SolveIt(const Graph& graph)
{
    Solver solver(graph);

    if (!graph.IsGenerated && !solver.IsSolvable())
    {
        LOG(ERROR) << "Cannot solve unsolvable puzzle";
    }
    else
    {
        solver.Solve();
        solver.PrintResult();
        solver.PrintStats();
    }
}

int main(int argc, const char **argv)
{
    SetUpLogger();
    START_EASYLOGGINGPP(argc, argv);

    SolveIt(GetGraph(GetArguments(argc, argv)));

    std::exit(EXIT_SUCCESS);
}
