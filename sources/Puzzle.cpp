/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Puzzle.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 20:56:22 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 11:43:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <set>
#include <numeric>
#include <cassert>
#include <easylogging++.h>

#include "Puzzle.hpp"

Puzzle::Puzzle(Traits::XYPosition::value_type size)
{
    //We're expecting to be able to fit the numbers into our type.
    assert(size * size < std::numeric_limits<cell_type>::max());

    //Resizing and filling the table.
    _data.resize(size);
    for (auto& line : _data)
        line.resize(size, 0);

    //Filling the table
    cell_type value = 0;
    const auto GetIt = [&value]() { return ++value; };
    for (auto limit = 0u; limit < size / 2; limit++)
    {
        //Upper line
        std::generate(_data[limit].begin() + limit,
                      _data[limit].end() - limit - 1, GetIt);

        //Right line
        for (auto ct = limit; ct < _data.size() - 1 - limit; ct++)
            _data[ct][_data.size() - 1 - limit] = ++value;

        //Lower line
        std::generate(_data[_data.size() - 1 - limit].rbegin() + limit,
                      _data[_data.size() - 1 - limit].rend() - limit - 1, GetIt);

        //This is how we skip entering size * size value into the 0 cell.
        if (value + 1 >= size * size)
            break;

        //Left line
        for (auto ct = _data.size() - 1 - limit; ct > limit; ct--)
            _data[ct][limit] = ++value;
    }
    
    _zeroPosition = {Traits::XYPosition::value_type(size / 2),
                     Traits::XYPosition::value_type(size / 2 + ((size % 2) - 1))};
}

bool Puzzle::valid() const
{
    if (_data.empty())
        return false;

    std::set<cell_type> uniqueSet;
    for (const auto& line : _data)
    {
        uniqueSet.insert(line.begin(), line.end());
    }
    const auto squareSize = _data.front().size();
    return uniqueSet.size() == squareSize * squareSize
            && *(uniqueSet.begin()) == 0;
}

Traits::XYPosition Puzzle::find(cell_type value) const
{
    assert(valid() && "How did you end up here?");

    // If we are looking for 0 and cached coordinates are ok,
    // just return them. No sense to update it, since all the use-cases
    // currently set _zeroPosition properly.
    if (value == 0)
        return _zeroPosition;

    Traits::XYPosition::value_type row = 0;

    for (const auto& line : _data)
    {
        const auto it = std::find(line.begin(), line.end(), value);
        if (it != line.end())
            return {row, Traits::XYPosition::value_type(std::distance(line.begin(), it))};
        row++;
    }

    return {std::numeric_limits<Traits::XYPosition::value_type>::max(),
            std::numeric_limits<Traits::XYPosition::value_type>::max()};
}

std::vector<Traits::Move> Puzzle::GetValidMoves() const
{
    assert(valid() && "How did you end up here?");
    std::vector<Traits::Move> mvs;

    const auto CheckNAdd = [this, &mvs](const Traits::Move d)
    {
        if (IsMoveValid(d))
            mvs.emplace_back(d);
    };
    CheckNAdd(Traits::Move::Up);
    CheckNAdd(Traits::Move::Right);
    CheckNAdd(Traits::Move::Down);
    CheckNAdd(Traits::Move::Left);

    return mvs;
}

bool Puzzle::IsMoveValid(const Traits::Move &d) const
{
    assert(valid() && "How did you end up here?");
    switch(d)
    {
        case Traits::Move::Up:
            return (_zeroPosition.row > 0);
        case Traits::Move::Left:
            return (_zeroPosition.col > 0);
        case Traits::Move::Down:
            return (_zeroPosition.row + 1 < _data.size());
        case Traits::Move::Right:
            return (_zeroPosition.col + 1 < _data.size());
        default:
            return false;
    }
}

Puzzle& Puzzle::operator+=(const Traits::Move& m)
{
    if (IsMoveValid(m))
        Move(m);

    return *this;
}

Puzzle& Puzzle::operator-=(const Traits::Move& m)
{
    if (IsMoveValid(!m))
        Move(!m);

    return *this;
}

std::istream &operator>>(std::istream &is, Puzzle & puzzle)
{
    std::vector<Puzzle::cell_type> temp;
    std::make_signed<Puzzle::cell_type>::type zeroPos = -1;//Poor man's std::optional

    while (!is.fail())
    {
        is >> std::ws;
        while ((is.peek() == '#') && !is.fail())
        {
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

        if (is.fail())
            break;

        Puzzle::cell_type t;
        is >> t;

        if (is.fail())
            break;

        if (t == 0)
        {
            zeroPos = temp.size() - 1;
        }
        temp.push_back(t);
    }

    if (temp.empty())
        return is;

    const auto squareSize = temp.front();
    temp.erase(temp.begin());

    //We're expecting to have the table smaller than the coordinates type can handle.
    //Some things can be broken, if the expectation is not true.
    assert(squareSize < std::numeric_limits<Traits::XYPosition::value_type>::max());

    //We're expecting to be able to fit the numbers into our type.
    assert(squareSize * squareSize < std::numeric_limits<Puzzle::cell_type>::max());

    if (temp.size() == squareSize * squareSize)
    {
        if (zeroPos >= 0)
        {
            puzzle._zeroPosition =
                {Traits::XYPosition::value_type(zeroPos / squareSize),
                Traits::XYPosition::value_type(zeroPos % squareSize)};
        }
        for (auto a = 0u; a < squareSize; ++a)
        {
            puzzle._data.emplace_back(temp.begin() + (a * squareSize), temp.begin() + (a * squareSize + squareSize));
        }
    }
    return is;
}

std::ostream &operator<<(std::ostream &os, const Puzzle &puzzle)
{
    const auto oldWidth = os.width();
    for (const auto &line : puzzle._data)
    {
        for (const auto &item : line)
        {
            os.width(3);
            if (item != 0)
                os << item;
            else
                os << " ";
            os << ' ';
        }
        os << std::endl;
    }
    os.width(oldWidth);
    return os;
}

std::string Puzzle::DumpRepresentation() const
{
    std::stringstream ss;

    ss << _data.size() << std::endl;

    for (const auto &line : _data)
    {
        for (const auto &item : line)
        {
            ss.width(3);
            ss << item << ' ';
        }
        ss << std::endl;
    }
    return ss.str();
}

void Puzzle::Move(const Traits::Move& d)
{
    //This is very strange, and basically should not happen
    assert(IsMoveValid(d) && "Is it even possible to be caught here?");

    auto newZeroIds = _zeroPosition;

    switch(d)
    {
        case Traits::Move::Up:
            newZeroIds.row--;
            break;
        case Traits::Move::Left:
            newZeroIds.col--;
            break;
        case Traits::Move::Down:
            newZeroIds.row++;
            break;
        case Traits::Move::Right:
            newZeroIds.col++;
            break;
    }

    std::swap(_data[_zeroPosition.row][_zeroPosition.col], _data[newZeroIds.row][newZeroIds.col]);
    _zeroPosition = newZeroIds;
}

std::size_t std::hash<Puzzle>::operator()(const Puzzle& p) const
{
    // FNV(Fowler-Noll-Vo) hash

    uint_fast64_t result = (sizeof(size_t) == 4) ? 2166136261u : 14695981039346656037u;
    const static uint_fast64_t prime = (sizeof(size_t) == 4) ? 16777619u : 1099511628211u;

    for (const auto& line : p.GetRepresentation())
        for (const auto& cell : line)
            result = (result * prime) ^ cell;

    return static_cast<std::size_t>(result);
}
