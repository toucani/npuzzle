/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Traits.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/21 08:55:01 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 11:48:43 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cassert>
#include "Traits.hpp"

using namespace Traits;

XYPosition::value_type XYPosition::Distance(const XYPosition& other) const
{
    const auto diff = *this - other;
    return diff.col + diff.row;
}

XYPosition XYPosition::operator+(const XYPosition& r) const
{
    return
    {
        static_cast<value_type>(row + r.row),
        static_cast<value_type>(col + r.col)
    };
}

XYPosition XYPosition::operator-(const XYPosition& r) const
{
    return
    {
        static_cast<value_type>(
            std::abs(static_cast<signed_value_type>(row)
                   - static_cast<signed_value_type>(r.row))),
        static_cast<value_type>(
            std::abs(static_cast<signed_value_type>(col)
                   - static_cast<signed_value_type>(r.col)))
    };
}

Move Traits::operator!(const Move& other)
{
    switch(other)
    {
        case Move::Up:    return Move::Down;
        case Move::Down:  return Move::Up;
        case Move::Left:  return Move::Right;
        case Move::Right: return Move::Left;
    }
    // gcc needs this not to complain about
    // "control reaches end of non-void function"
    assert(false);
    return Move::Left;
}
