#!/bin/bash

FILES="solvable/puzzle3-1.in
solvable/puzzle3-2.in
solvable/puzzle3-3.in
solvable/puzzle3-4.in
solvable/puzzle3-5.in
solvable/puzzle4-1.in
solvable/puzzle4-2.in
solvable/puzzle4-3.in
solvable/puzzle4-4.in
solvable/puzzle4-5.in
solvable/puzzle5-1.in
solvable/puzzle5-2.in
solvable/puzzle5-3.in
solvable/puzzle5-4.in
solvable/puzzle5-5.in"

for ARG in $FILES
do
    RES=$(./../npuzzle -f "$ARG" | tail -n 4 | grep "Time taken:")
    echo "$ARG -> $RES"
done
