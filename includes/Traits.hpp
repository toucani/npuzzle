/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Traits.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/12 13:11:20 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 11:29:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <cstdint>

namespace Traits
{
    namespace Puzzle
    {
        using cell_type = uint_fast16_t;
        using line_type = std::vector<cell_type>;
        using container_type = std::vector<line_type>;
    }

    /*!
    * This class represents a single tile position.
    */
    struct XYPosition
    {
        using value_type = uint_fast16_t;
        using signed_value_type = std::make_signed<value_type>::type;

        XYPosition() = default;
        XYPosition(value_type row, value_type col) : row(row), col(col) {}

        //Manhattan distance
        value_type Distance(const XYPosition& other) const;

        bool operator!=(const XYPosition& r) const { return !operator==(r); }
        bool operator==(const XYPosition& r) const { return row == r.row && col == r.col; }

        XYPosition operator+(const XYPosition& r) const;
        XYPosition operator-(const XYPosition& r) const;

        value_type row = 0, col = 0;
    };

    /*!
    * This represents a single move, which can be apllied to any
    * valid puzzle. An edge node for the graph.
    */
    enum class Move { Up, Down, Left, Right };
    Move operator!(const Move& other);
};
