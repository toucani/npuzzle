/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logs.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 20:30:45 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/21 15:26:24 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <easylogging++.h>

#define VLogReport(level, msg) VLogReporter __vlrep(level, msg);

namespace VLogLevels
{
    const el::base::type::VerboseLevel Args = 8;
    const el::base::type::VerboseLevel Solver = 6;
    const el::base::type::VerboseLevel Factory = 4;
    const el::base::type::VerboseLevel Puzzle = 2;
};

class VLogReporter final
{
public:
    VLogReporter() = delete;
    VLogReporter(el::base::type::VerboseLevel vlevel, std::string message)
        : _vlevel(vlevel), _message(message)
    {
        VLOG(_vlevel) << "-- " << _message << "...";
    }

    ~VLogReporter()
    {
        _message[0] = std::tolower(_message[0]);
        VLOG(_vlevel) << "-- Finished " << _message << ".";
    }

private:
    el::base::type::VerboseLevel _vlevel;
    std::string _message;
};
