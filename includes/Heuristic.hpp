/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Heuristic.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 12:47:51 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 11:57:02 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <functional>

class Puzzle;

/*!
 * This is heuristic, needed by algorithm. Heuristics measure the distance
 * between current solution, and target solution. The bigger distance is -
 * the worse the solution is.
 */
namespace Heuristic
{
    enum Type
    {
        Manhatton,
        Snake,
        Mean,
        Euclidian
    };

    using value_type = uint_fast64_t;
    using interface = std::function<value_type(const Puzzle& current, const Puzzle& target)>;

    value_type ManhattanDistance(const Puzzle& current, const Puzzle& target);
    value_type SnakeDistance(const Puzzle& current, const Puzzle& target);
    value_type MeanSquaredDistance(const Puzzle& current, const Puzzle& target);
    value_type EuclidianDistance(const Puzzle& current, const Puzzle& target);
}
