/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Solver.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 21:58:32 by dkovalch          #+#    #+#             */
/*   Updated: 2018/07/30 21:46:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <chrono>
//This is needed to make friends with tests...
#include <../externals/gtest/googletest/include/gtest/gtest_prod.h>

#include "Graph.hpp"

class Solver
{
public:
    Solver() = delete;
    Solver(const Graph& g) : _graph(g) {}

    static bool IsSolvable(const Puzzle&);
    bool IsSolvable() const {return IsSolvable(_graph.GetStart()); }
    void Solve();

    void PrintResult() const;
    void PrintStats() const;

private:
    //This structure is used to enable sorting
    //by heuristic rating in priority queue.
    struct PriorityPuzzle
    {
        PriorityPuzzle() = default;
        PriorityPuzzle(const Puzzle& p, Heuristic::value_type r)
            : puzzle(p), rating(r) {};

        const auto& GetPuzzle() const { return puzzle; }
        auto GetHeuristic() const { return rating; }

        bool operator<(const PriorityPuzzle& other) const { return rating > other.rating; }
        bool operator>(const PriorityPuzzle& other) const { return rating < other.rating; }
        bool operator==(const PriorityPuzzle& other) const
        {
            return rating == other.rating && puzzle == other.puzzle;
        }

    private:
        Puzzle puzzle;
        Heuristic::value_type rating = 0;
    };

    static Puzzle::container_type::size_type GetInversionCount(const Puzzle::container_type&);

    Graph _graph;
    std::vector<Puzzle> _result;
    size_t _complTime = 0;
    size_t _complMem = 0;
    std::chrono::milliseconds _time;

    FRIEND_TEST(Solver, InversionCount);
};
