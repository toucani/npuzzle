/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Puzzle.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 17:56:33 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/19 17:40:39 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <iostream>
//This is needed to make friends with tests...
#include <../externals/gtest/googletest/include/gtest/gtest_prod.h>

#include "Traits.hpp"

/*!
 * This class represents a single puzzle state. A node for the graph.
 */
class Puzzle final
{
public:
    using cell_type = Traits::Puzzle::cell_type;
    using line_type = Traits::Puzzle::line_type;
    using container_type = Traits::Puzzle::container_type;

    Puzzle() = default;

    /*!
     * Constructs properly(snake solution)filled puzzle.
     * @param size width(or height) of the square.
     */
    explicit Puzzle(Traits::XYPosition::value_type size);

    /*!
     * Checks if the puzzle is valid.
     * @return true - if the puzzle is valid, false otherwise
     */
    bool valid() const;

    /*!
     * Checks if the puzzle is empty.
     * @return true if empty, false otherwise
     */
    bool empty() const { return _data.empty(); }

    /*!
     * Returns square size(height or width)
     * @return square size
     */
    auto size() const { return _data.size(); }

    /*!
     * Finds the position of the tile with the given value.
     * @return position of the tile, or
     * std::numeric_limits<Traits::XYPosition::value_type>::max() if not found.
     */
    Traits::XYPosition find(cell_type) const;

    /*!
     * Returns data as is in current state.
     * @return the data itself.
     */
    const container_type& GetRepresentation() const { return _data; }

    /*!
     * Gathers all valid moves(up to 4).
     * @return vector of valid moves(up to 4).
     */
    std::vector<Traits::Move> GetValidMoves() const;

    /*!
     * Checks if such move can be applied. There is no sense to use if
     * for the moves obtained thru GetValidMoves().
     * @param move move to check
     * @return true if it can be applied, false otherwise
     */
    bool IsMoveValid(const Traits::Move& m) const ;

    bool operator!=(const Puzzle &rhs) const { return !(rhs == *this); }
    bool operator==(const Puzzle &rhs) const { return _data == rhs._data; }

    /*!
     * Applies a move to a copy of the puzzle.
     * @param move move to apply(add) or undo(substract)
     * @return new puzzle with applied/undone move.
     */
    const Puzzle operator+(const Traits::Move& m) const { return Puzzle(*this) += m; }
    const Puzzle operator-(const Traits::Move& m) const { return Puzzle(*this) -= m; }
    Puzzle& operator+=(const Traits::Move& m);
    Puzzle& operator-=(const Traits::Move& m);

    friend std::istream &operator>>(std::istream &, Puzzle &);
    friend std::ostream &operator<<(std::ostream &, const Puzzle &);

private:
    /*!
     * Returns stringified representation. Is used for testing.
     * Should not be used for printing.
     * @return stringified data.
     */
    std::string DumpRepresentation() const;

    /*!
     * Actually moves 0 tile in given direction.
     */
    void Move(const Traits::Move& d);

    container_type _data;
    Traits::XYPosition _zeroPosition;

    FRIEND_TEST(Puzzle, WriteAndRead);
    FRIEND_TEST(Puzzle, Read);
};

template <>
struct std::hash<Puzzle>
{
    std::size_t operator()(const Puzzle&) const;
};
