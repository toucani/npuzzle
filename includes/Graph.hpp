/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Graph.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/16 17:42:29 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 11:41:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Puzzle.hpp"
#include "Heuristic.hpp"

/*!
 * This class represents entire ready-to-solve graph.
 * Nodes are called Puzzle, edges - Moves.
 * We're solving it from Start to Finish.
 */
class Graph
{
public:
    Graph() = delete;
    Graph(Puzzle start, Puzzle finish,
          Heuristic::Type heuristicId = Heuristic::Type::Manhatton, bool generated = false)
        : IsGenerated(generated),
          _start(std::move(start)),
          _finish(std::move(finish)),
          _heuristic(heuristicId)
        {};

    static std::vector<Puzzle> GetNeighbours(const Puzzle&);

    const auto& GetStart() const { return _start; }
    const auto& GetFinish() const { return _finish; }

    template<typename ... Args>
    decltype(auto) Heuristic(Args ... args) const
    {
        switch(_heuristic)
        {
            case Heuristic::Snake:
                return Heuristic::SnakeDistance(std::forward<Args>(args)...);
            case Heuristic::Mean:
                return Heuristic::MeanSquaredDistance(std::forward<Args>(args)...);
            case Heuristic::Euclidian:
                return Heuristic::EuclidianDistance(std::forward<Args>(args)...);
            default:
                return Heuristic::ManhattanDistance(std::forward<Args>(args)...);
        }
    }

    const bool IsGenerated = false;
private:
    Puzzle _start, _finish;
    const Heuristic::Type _heuristic = Heuristic::Type::Manhatton;
};
