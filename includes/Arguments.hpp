/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Arguments.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 21:17:10 by dkovalch          #+#    #+#             */
/*   Updated: 2018/12/21 12:05:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of npuzzle project.
**   Copyright (C) 2018,
**     Dmytro Kovalchuk (dkovalch@student.unit.ua),
**     Olha Kosiakova (okosiako@student.unit.ua).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <vector>
#include <ostream>

class Arguments
{
public:
    Arguments(int argc, const char **argv);

    bool help() const { return _help; }
    int heuristic() const { return _heuristic; }
    int size() const { return _size; }
    int shuffle() const { return _shuffle; }
    const std::string& filename() const { return _filename; }

    static std::string PrintHelp();
    friend std::ostream &operator<<(std::ostream &, const Arguments &);

private:
    bool GetFlagValue(const std::vector<std::string>& args,
                        const std::string& shortOpt,
                        const std::string& longOpt,
                        bool defaultValue);
    std::string GetArgValue(const std::vector<std::string>& args,
                        const std::string& shortOpt,
                        const std::string& longOpt,
                        const std::string& defaultValue = "");

    int GetArgValue(const std::vector<std::string>& args,
                        const std::string& shortOpt,
                        const std::string& longOpt,
                        int defaultValue);

    bool _help = false;
    std::string _filename = "";
    int _heuristic = 0;
    int _size = -1;
    int _shuffle = -1;
};
