# N-puzzle

N-puzzle is a N-puzzle ("taquin" in French) game solver. This program tries to
find a series of moves to have a "snake solution" for the given(or generated) puzzle.
Puzzle files can be given to the program via ```-f``` flag, while not
giving a puzzle to the program will force it to generate one.
Run the program with ```--help``` to see all the command line flags.

## Installing

Clone this repository recursively:
```
git clone --recurse-submodules
```
, move into the folder
```
cd n-puzzle && mkdir build && cd build
```
and
```
cmake .. && make
```

It can be build with sanitizers, just by using:
```
cmake .. -DSANITIZE_ADDRESS=On && make
```
, or
```
cmake .. -DSANITIZE_MEMORY=On && make
```
, or
```
cmake .. -DSANITIZE_THREAD=On && make
```
, or
```
cmake .. -DSANITIZE_UNDEFINED=On && make
```
instead of the last step.


## Using
Such arguments specify logging behaviour(see [EasyLogging++](https://github.com/muflihun/easyloggingpp#application-arguments)):

|        Argument            |                                      Description                                        |
|----------------------------|-----------------------------------------------------------------------------------------|
| `-v`                       | Activates maximum verbosity                                                             |
| `--v=2`                    | Activates verbosity upto verbose level 2 (valid range: 0-9)                             |
| `--verbose`                | Activates maximum verbosity                                                             |
| `--logging-flags=3`        | Sets logging flag. In example `i.e, 3`, it sets logging flag to `NewLineForContainer` and `AllowVerboseIfModuleNotSpecified`. See logging flags section above for further details and values. See macros section to disable this function.|
| `--default-log-file=FILE`  |Sets default log file for existing and future loggers. You may want to consider defining `ELPP_NO_DEFAULT_LOG_FILE` to prevent creation of default empty log file during pre-processing. See macros section to disable this function.

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Authors

* [Dmytro Kovalchuk](https://bitbucket.org/Mitriksicilian/)
* [Olha Kosiakova](https://bitbucket.org/okosiako/)

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=n-puzzle from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to [sanitizers for cmake](https://github.com/arsenm/sanitizers-cmake/)
* to [Google test](https://github.com/google/googletest)
* to [EasyLogging++](https://github.com/muflihun/easyloggingpp)
* to UNIT Factory, for inspiration to do our best.
* to all UNIT Factory students, who shared their knowledge and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
